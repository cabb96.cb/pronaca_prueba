# Importamos las librerias con las cuales se va a trabajar con Python
import pandas as pd
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.model_selection import cross_validate
from sklearn.tree import DecisionTreeRegressor
from sklearn import preprocessing
import numpy as np
from sklearn.preprocessing import StandardScaler

import Conexion

def show_nulls(df):
    # Nulos totales
    total_nan = df.isnull().sum()
    # Porcentaje nulos
    perc_nan = total_nan/len(df)*100
    
    return total_nan, perc_nan

def main():

    # Dataframe de entrenamiento

    df_pronaca_entrenamiento = pd.read_excel("pronaca_acp.xlsx")
    df_pronaca_entrenamiento = df_pronaca_entrenamiento[['prov', 'tasa_empleo_inadecuado', 'coef_gini', 
                             'tasa_incidencia_pobreza', 'd_poblacion', 'porcentaje_poblacion_d', 
                             'c_menos_poblacion', 'poblacion', 'tasa_desempleo', 'prom_vta_mensual']]
    df_pronaca_entrenamiento['prov'] = pd.Categorical(df_pronaca_entrenamiento.prov)

    # Dataframe de prediccion

    df_localidades_predicion = pd.read_sql('select * from public.localidades', Conexion.cursor_stage_postgres)
    df_localidades_predicion = df_localidades_predicion[['dpa_manzan', 'tasa_empleo_inadecuado', 
                                                         'coef_gini', 'tasa_incidencia_pobreza', 'd_poblacion', 
                                                         'porcentaje_poblacion_d', 'c-_poblacion', 'poblacion', 
                                                         'tasa_desempleo', 
                                                         'facturacion_de_cerdo_en_la_zona_de_influencia',
                                                         'facturacion_de_embutidos_en_la_zona_de_influencia',
                                                         'facturacion_de_huevos_en_la_zona_de_influencia',
                                                         'facturacion_de_pollo_en_la_zona_de_influencia',
                                                         'facturacion_de_res_en_la_zona_de_influencia']]
    df_localidades_predicion['dpa_parroq'] =  df_localidades_predicion['dpa_manzan'].str.slice(0, 6)
    df_localidades_predicion['facturacion_vta_mensual'] = df_localidades_predicion['facturacion_de_cerdo_en_la_zona_de_influencia'].astype(float) + df_localidades_predicion['facturacion_de_embutidos_en_la_zona_de_influencia'].astype(float) + df_localidades_predicion['facturacion_de_huevos_en_la_zona_de_influencia'].astype(float) + df_localidades_predicion['facturacion_de_pollo_en_la_zona_de_influencia'].astype(float) + df_localidades_predicion['facturacion_de_res_en_la_zona_de_influencia'].astype(float)
    df_localidades_predicion = df_localidades_predicion[['tasa_empleo_inadecuado', 
                                                         'coef_gini', 'tasa_incidencia_pobreza', 'd_poblacion', 
                                                         'porcentaje_poblacion_d', 'c-_poblacion', 'poblacion', 
                                                         'tasa_desempleo','dpa_parroq', 'facturacion_vta_mensual']]

    df_dpa_parroquias = pd.read_sql('select * from public.dpa_parroquias', Conexion.cursor_stage_postgres)
    df_dpa_parroquias = df_dpa_parroquias[['dpa_parroq', 'dpa_despro']]

    df_localidades_produccion_completo = pd.merge(df_localidades_predicion, df_dpa_parroquias, on= "dpa_parroq")
    df_localidades_produccion_completo = df_localidades_produccion_completo.drop('dpa_parroq', axis=1)
    df_localidades_produccion_completo = df_localidades_produccion_completo[df_localidades_produccion_completo["facturacion_vta_mensual"].notna()]
    
    df_localidades_produccion_completo['prov'] = pd.Categorical(df_localidades_produccion_completo.dpa_despro)
    
    enc = preprocessing.OrdinalEncoder()
    df_pronaca_entrenamiento['cod_prod'] = enc.fit_transform(df_pronaca_entrenamiento[['prov']])
    df_prov_reco = pd.DataFrame()
    df_prov_reco = df_pronaca_entrenamiento[['cod_prod', 'prov']]

    var_x = df_pronaca_entrenamiento[['cod_prod', 'tasa_empleo_inadecuado', 'coef_gini', 
                             'tasa_incidencia_pobreza', 'd_poblacion', 'porcentaje_poblacion_d', 
                             'c_menos_poblacion', 'poblacion', 'tasa_desempleo']]
    
    var_y = df_pronaca_entrenamiento[['prom_vta_mensual']]

    scaler = StandardScaler()
    scaler.fit(var_x)
    var_xs = scaler.transform(var_x)
    df_var_xs=pd.DataFrame(var_xs)
    df_var_xs.columns=var_x.columns
    X_train, X_test, y_train, y_test = train_test_split(var_xs, var_y, random_state=5)

    #Se ejecuta una regresion knn, se determina que no es un buen modelo
    m1_knn=KNeighborsRegressor()
    cv_results= cross_validate(m1_knn, X_train, y_train, cv=5, return_estimator=True)
    indice_mejor = np.argmax(cv_results['test_score'])
    print(f"Se ejecuta una regresion knn, se determina que no es un buen modelo {indice_mejor}" )

    #Se ejecuta un modelo de machine learning de arbol de decision regresion, se determina que es un mejor modelo para predecir
    modelo_regression_tree = DecisionTreeRegressor(random_state=5)
    modelo_regression_tree.fit(X_train, y_train)
    m3_tr=DecisionTreeRegressor()
    cv_results= cross_validate(m3_tr, X_train, y_train, cv=5, return_estimator=True)
    indice_mejor = np.argmax(cv_results['test_score'])
    print(f"Se ejecuta un modelo de machine learning de arbol de decision regresion, se determina que es un mejor modelo para predecir {indice_mejor}" )

    tr_mejor = cv_results['estimator'][indice_mejor]
    tr_mejor.predict(X_test)
    print(tr_mejor.predict(X_test))

    #Prediccion de ventas
    # df_localidades_predicion.info()
    # print(df_localidades_predicion.head(5))
    # df_dpa_parroquias.info()
    # print(df_dpa_parroquias.head(5))
    df_localidades_predicion_completo = pd.merge(df_localidades_predicion, df_dpa_parroquias, on= "dpa_parroq")
    df_localidades_predicion_completo['prov'] = pd.Categorical(df_localidades_predicion_completo.dpa_despro)
    df_localidades_predicion_completo['cod_prod'] = enc.fit_transform(df_localidades_predicion_completo[['prov']])
    df_localidades_predicion_completo.info()
    var_pred_x = df_localidades_predicion_completo[['cod_prod', 'tasa_empleo_inadecuado', 'coef_gini', 
                             'tasa_incidencia_pobreza', 'd_poblacion', 'porcentaje_poblacion_d', 
                             'c-_poblacion', 'poblacion', 'tasa_desempleo']]
    
    var_pred_y = df_localidades_predicion_completo[['facturacion_vta_mensual']]

    scaler.fit(var_pred_x)
    var_pred_xs = scaler.transform(var_pred_x)
    df_var_pred_xs=pd.DataFrame(var_pred_xs)
    df_var_pred_xs.columns=var_pred_x.columns


    YP3 = tr_mejor.predict(df_var_pred_xs)
    YP3=np.round(YP3,2)
    df_localidades_predicion_completo['PREDICCION_VENTA'] = YP3
    nombre_archivo = "LocalidadesPredicionCompleto.xlsx"
    df_localidades_predicion_completo.to_excel(nombre_archivo)
    print(df_localidades_predicion_completo)





if __name__ == '__main__':
    main()